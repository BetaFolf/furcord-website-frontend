import VuetifyLoaderPlugin from 'vuetify-loader/lib/plugin'

require("dotenv").config();

const axiosConfig = () => {
  if (process.env.PRODUCTION === "false") {
    return {
      baseURL: "https://192.168.23.134/api/",
      https: true,
      retry: { retries: 3 },
      progress: true
    }
  } else {
    return {
      baseURL: "https://furcord.net/api/",
      https: true,
      retry: { retries: 3 },
      progress: true
    }
  }
}

export default {
  server: {
    host: "0.0.0.0",
    port: 3000
  },
  mode: 'spa',
  head: {
    title: "Furcord",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: "Furcord is a Discord server by furries, for furries (and non furries)!" }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/media/transparent.png' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/fonts/roboto.css' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/globals.css' }
    ]
  },

  loading: "~/components/loading.vue",

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],

  axios: axiosConfig(),

  plugins: [{
    src: '~/plugins/vuex-persisted.js', ssr: false
  }],

  build: {
    transpile: ['vuetify/lib'],
    plugins: [new VuetifyLoaderPlugin()],
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
